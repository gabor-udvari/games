;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019, 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages unigine)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg))

(define-public unigine-heaven
  (let* ((arch (if (target-64bit?)
                 "x64"
                 "x86"))
         (browser (string-append "browser_" arch))
         (heaven (string-append "heaven_" arch)))
    (package
      (name "unigine-heaven")
      (version "4.0")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://assets.unigine.com/d/Unigine_Heaven-"
                             version ".run"))
         (sha256
          (base32
           "19rndwwxnb9k2nw9h004hyrmr419471s0fp25yzvvc6rkd521c0v"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f           ; Already stipped.
         #:patchelf-plan
         `((,,(string-append "bin/" browser)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/" heaven)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libUnigine_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxext" "libxinerama" "libxrender"))
           (,,(string-append "bin/libAppStereo_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppSurround_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppWall_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libGPUMonitor_" arch ".so")
            ("libc" "gcc" "libx11" "libxext" "out"))
           (,,(string-append "bin/libQtCoreUnigine_" arch ".so.4")
            ("libc" "gcc" ))
           (,,(string-append "bin/libQtGuiUnigine_" arch ".so.4")
            ("libc" "gcc" "libxrender" "libxext" "libx11" "freetype"
             "fontconfig" "out"))
           (,,(string-append "bin/libQtNetworkUnigine_" arch ".so.4")
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libQtWebKitUnigine_" arch ".so.4")
            ("libc" "gcc" "fontconfig" "freetype" "libxext" "libx11" "libxrender" "out"))
           (,,(string-append "bin/libQtXmlUnigine_" arch ".so.4")
            ("libc" "gcc" "out")))
         #:install-plan
         `(("bin" (,,(string-append "browser_" arch)
                   ,,(string-append "heaven_" arch))
            "share/unigine-heaven/bin/")
           ("bin" (,,(string-append "lib.*" arch ".*"))
            "lib/")
           ("data" (".*") "share/unigine-heaven/data/")
           ("documentation" (".*") "share/doc/unigine-heaven/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "bash")
                       (assoc-ref inputs "source"))
               (chdir (string-append "Unigine_Heaven-" ,version))
               #t))
           (add-after 'install 'wrap-program
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/unigine-heaven"))
                      (wrapper (string-append out "/bin/heaven")))
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a/bin/bash~%" (which "bash"))
                     (format #t "export LD_LIBRARY_PATH=~a~%" (string-append
                                                               (assoc-ref inputs "openal") "/lib:"
                                                               (assoc-ref inputs "mesa") "/lib"))
                     (format #t "cd ~a~%" (string-append share "/bin"))
                     (format #t "~a/bin/browser_~a -config ~a/data/launcher/launcher.xml"
                             share ,arch share)))
                 (chmod wrapper #o755))
               #t)))))
      (inputs
       `(("gcc" ,(@@ (gnu packages gcc) gcc-9) "lib")
         ("libx11" ,libx11)
         ("libxrandr" ,libxrandr)
         ("libxext" ,libxext)
         ("libxinerama" ,libxinerama)
         ("libxrender" ,libxrender)
         ("freetype" ,freetype)
         ("fontconfig" ,fontconfig)
         ("mesa" ,mesa)
         ("openal" ,openal)))
      (home-page "https://benchmark.unigine.com/heaven")
      (synopsis "GPU-intensive benchmark focused on tessellation")
      (description "This benchmark immerses a user into a magical steampunk
world of shiny brass, wood and gears.  Nested on flying islands, a tiny village
with its cozy, sun-heated cobblestone streets, and a majestic dragon on the
central square gives a true sense of adventure.  An interactive experience with
fly-by and walk-through modes allows for exploring all corners of this world
powered by the UNIGINE Engine that leverages advanced capabilities of graphics
APIs and turns this bench into a visual masterpiece.")
      (license (nonfree "No URL")))))

(define-public unigine-valley
  (let* ((arch (if (target-64bit?)
                 "x64"
                 "x86"))
         (browser (string-append "browser_" arch))
         (engine (string-append "valley_" arch)))
    (package
      (name "unigine-valley")
      (version "1.0")
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://assets.unigine.com/d/Unigine_Valley-"
                             version ".run"))
         (sha256
          (base32
           "1x306r24dxfa1lj9g5kq81x4xc8gqbqzbfxsh88ma60i8g98n32z"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f           ; Already stipped.
         #:patchelf-plan
         `((,,(string-append "bin/" browser)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/" engine)
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libUnigine_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxext" "libxinerama" "libxrender"))
           (,,(string-append "bin/libAppStereo_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppSurround_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libAppWall_" arch ".so")
            ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "out"))
           (,,(string-append "bin/libGPUMonitor_" arch ".so")
            ("libc" "gcc" "libx11" "libxext" "out"))
           (,,(string-append "bin/libQtCoreUnigine_" arch ".so.4")
            ("libc" "gcc" ))
           (,,(string-append "bin/libQtGuiUnigine_" arch ".so.4")
            ("libc" "gcc" "libxrender" "libxext" "libx11" "freetype"
             "fontconfig" "out"))
           (,,(string-append "bin/libQtNetworkUnigine_" arch ".so.4")
            ("libc" "gcc" "out"))
           (,,(string-append "bin/libQtWebKitUnigine_" arch ".so.4")
            ("libc" "gcc" "fontconfig" "freetype" "libxext" "libx11" "libxrender" "out"))
           (,,(string-append "bin/libQtXmlUnigine_" arch ".so.4")
            ("libc" "gcc" "out")))
         #:install-plan
         `(("bin" (,,(string-append "browser_" arch)
                   ,,(string-append "valley_" arch))
            "share/unigine-valley/bin/")
           ("bin" (,,(string-append "lib.*" arch ".*"))
            "lib/")
           ("data" (".*") "share/unigine-valley/data/")
           ("documentation" (".*") "share/doc/unigine-valley/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "bash")
                       (assoc-ref inputs "source"))
               (chdir (string-append "Unigine_Valley-" ,version))
               #t))
           (add-after 'install 'wrap-program
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/unigine-valley"))
                      (wrapper (string-append out "/bin/valley")))
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a/bin/bash~%" (which "bash"))
                     (format #t "export LD_LIBRARY_PATH=~a~%" (string-append
                                                               (assoc-ref inputs "openal") "/lib:"
                                                               (assoc-ref inputs "mesa") "/lib"))
                     (format #t "cd ~a~%" (string-append share "/bin"))
                     (format #t "~a/bin/browser_~a -config ~a/data/launcher/launcher.xml"
                             share ,arch share)))
                 (chmod wrapper #o755))
               #t)))))
      (inputs
       `(("gcc" ,(@@ (gnu packages gcc) gcc-9) "lib")
         ("libx11" ,libx11)
         ("libxrandr" ,libxrandr)
         ("libxext" ,libxext)
         ("libxinerama" ,libxinerama)
         ("libxrender" ,libxrender)
         ("freetype" ,freetype)
         ("fontconfig" ,fontconfig)
         ("mesa" ,mesa)
         ("openal" ,openal)))
      (home-page "https://benchmark.unigine.com/valley")
      (synopsis "GPU-intensive benchmark focused on vegetation")
      (description "The forest-covered valley surrounded by vast mountains
amazes with its scale from a bird’s-eye view and is extremely detailed down to
every leaf and flower petal.  Valley Benchmark allows you to encounter a morning
high up in the mountains when the snow-capped peaks are just barely glittering
in the rising sun.  Be it flying over the vast green expanses or hiking along
rocky slopes, this journey continues as long as you wish.  Unique in every
corner, this open-space world provides a wonderfully relaxing experience under
the tranquil music and sounds of nature.")
      (license (nonfree "No URL")))))

;; TODO: According to upstream, the launcher requires Qt 5.7.
;; For now, launcher fails to start with:
;; $ /gnu/store/3sdcf0rnqyx4dzszs5svz3hpw9dqfahh-unigine-superposition-1.1/Superposition
;; QQmlApplicationEngine failed to load component
;; qrc:/gui/main.qml:113 Type Icon unavailable
;; qrc:/gui/components/Icon.qml:10 Cannot override FINAL property
(define-public unigine-superposition
  (package
    (name "unigine-superposition")
    (version "1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://assets.unigine.com/d/Unigine_Superposition-"
                           version ".run"))
       (sha256
        (base32
         "12hzlz792pf8pvxf13fww3qhahqzwzkxq9q3mq20hbhvaphbg7nd"))))
    (build-system binary-build-system)
    (supported-systems '("x86_64-linux"))
    (arguments
     `(#:strip-binaries? #f             ; Already stipped.
       #:patchelf-plan
       `(;; ("Superposition"
         ;;  ("libc" "gcc"))
         ;; ("bin/launcher"
         ;;  ("libc" "gcc" "libx11" "libxinerama" "libxext" "mesa"
         ;;   "qtbase" "qtdeclarative" "qtwebsockets" "out"))
         ("bin/libAppVive_x64.so"
          ("libc" "gcc" "libx11" "libxrandr" "libxinerama" "openvr"
           "qtbase" "qtdeclarative" "qtwebsockets" "out"))
         ("bin/libGPUMonitor_x64.so"
          ("libc" "gcc" "libx11" "libxext" "out"))
         ("bin/libUnigine_x64.so"
          ("libc" "gcc" "libx11" "libxrandr" "libxext" "libxinerama" "libxrender"))
         ("bin/superposition"
          ("libc" "gcc" "out"))
         ("bin/superposition_cli"
          ("libc" "gcc" "out")))
       #:install-plan
       `(;; ("Superposition" #f "./")
         ;; ("bin" ("launcher") "share/unigine-superposition/bin/")
         ;; ("bin" ("superposition_cli") "bin/")
         ("bin/superposition" #f "share/unigine-superposition/")
         ("bin" ("libAppVive_x64.so"
                 "libGPUMonitor_x64.so"
                 "libUnigine_x64.so")
          "lib/")
         ("bin/pro_xml_samples" (".") "share/unigine-superposition/bin/pro_xml_samples/")
         ;; TODO: Use Guix'?
         ;; ("bin/qt" ("qml") "bin/qt/")
         ;; ("bin/qt" (".") "./")
         ;; ("bin/qt" (".") "bin/qt/")
         ;; ("bin/qt.conf" #f "bin/")
         ("Superposition.png" #f "share/unigine-superposition/")
         ("version" #f "share/unigine-superposition/")
         ("icons" (".") "share/unigine-superposition/icons/")
         ("data" (".") "share/unigine-superposition/data/")
         ("docs" (".") "share/unigine-superposition/docs/"))
       #:phases
       (modify-phases %standard-phases
         (replace 'unpack
           (lambda* (#:key inputs #:allow-other-keys)
             (invoke (which "bash")
                     (assoc-ref inputs "source")
                     "--noexec")
             (chdir (string-append "Unigine_Superposition-" ,version))
             #t))
         (add-after 'install 'wrap-executable
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (wrap-program (string-append out "/share/unigine-superposition/bin/superposition")
                 `("LD_LIBRARY_PATH" ":" prefix
                   ;; `bin/superposition' loads libGL and libopenal at runtime.
                   ;; This can be seen by running it directly with a random argument.
                   ,(map (lambda (label)
                           (string-append (assoc-ref inputs label)
                                          "/lib"))
                         '("mesa" "openal"))))
               #t)))
         (add-after 'install 'make-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (real (string-append out "/share/unigine-superposition/bin/superposition"))
                    (wrapper (string-append out "/bin/superposition")))
               (mkdir-p (dirname wrapper))
               (call-with-output-file wrapper
                 (lambda (p)
                   (format p
                           (string-append
                            "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                            "exec -a " (basename wrapper) " " real " -sound_app openal \\" "\n"
                            " -system_script superposition/system_script.cpp \\" "\n"
                            " -data_path ../ -engine_config ../data/superposition/unigine.cfg \\" "\n"
                            " -video_mode -1 -project_name Superposition \\" "\n"
                            " -video_resizable 1 \\" "\n"
                            " -console_command \"config_readonly 1 && world_load superposition/superposition\" \\" "\n"
                            " -mode 2 -preset 0 \"$@\"" "\n"))))
               (chmod wrapper #o755)
               #t))))))
    (inputs
     `(("bash" ,bash)
       ("gcc" ,gcc "lib")
       ("libx11" ,libx11)
       ("libxrandr" ,libxrandr)
       ("libxext" ,libxext)
       ("libxinerama" ,libxinerama)
       ("libxrender" ,libxrender)
       ("freetype" ,freetype)
       ("fontconfig" ,fontconfig)
       ("mesa" ,mesa)
       ("openal" ,openal)
       ;; ("openssl" ,openssl-1.0) ; In the bin/qt folder, but not sure it's actually needed.
       ;; ("icu4c" ,icu4c) ; In the bin/qt folder, but not sure it's actually needed.
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtwebsockets" ,qtwebsockets)
       ;; ("qtquickcontrols" ,qtquickcontrols)
       ;; ("qtgraphicaleffects" ,qtgraphicaleffects)
       ;; ("qtlocation" ,qtlocation)
       ("openvr" ,openvr)))
    (home-page "https://benchmark.unigine.com/superposition")
    (synopsis "GPU-intensive benchmark")
    (description "A lone professor performs dangerous experiments in an
abandoned classroom, day in and day out.  Obsessed with inventions and
discoveries beyond the wildest dreams, he strives to prove his ideas.  Once you
come to this place in the early morning, you would not meet him there.  The
eerie thing is a loud bang from the laboratory heard a few moments ago.  What
was that?  You have the only chance to cast some light upon this incident by
going deeply into the matter of quantum theory: thorough visual inspection of
professor's records and instruments will help to lift the veil on the mystery.")
    (license (nonfree "No URL"))))
