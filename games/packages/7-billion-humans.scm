;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages 7-billion-humans)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages sdl)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public gog-7-billion-humans
  (let ((buildno "23774")
        (binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "7BillionHumans.bin.x86_64")
                  ("i686-linux" "7BillionHumans.bin.x86")
                  (_ ""))))
    (package
      (name "gog-7-billion-humans")
      (version "1.0.32487")
      (source
       (origin
        (method gog-fetch)
        (uri "gogdownloader://7_billion_humans/en3installer0")
        (file-name (string-append "7_billion_humans_"
                                  (string-replace-substring version "." "_")
                                  "_" buildno ".sh"))
        (sha256
         (base32
          "0kc1pg8cnf05vw5x7809lgf964i3qn0b1bl9gjgzw3xi0qbb8wb1"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "gcc:lib" "libstdc++" "openal" "sdl2" "zlib")))
         #:phases
         (modify-phases %standard-phases
           (add-before 'install 'delete-bundled-libs
             (lambda _
               (delete-file-recursively "data/noarch/game/lib")
               (delete-file-recursively "data/noarch/game/lib64")
               #t)))))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("libstdc++" ,(make-libstdc++ gcc))
         ("openal" ,openal)
         ("sdl2" ,sdl2)
         ("zlib" ,zlib)))
      (home-page "https://tomorrowcorporation.com/7billionhumans")
      (synopsis "Automate swarms of office workers to solve puzzles")
      (description "Automate swarms of office workers to solve puzzles inside
your very own parallel computer made of people.  A thrilling followup to the
award-winning Human Resource Machine.  Now with more humans!

@itemize
@item
More puzzles, more humans, more rippling brain muscles - over 60+ levels of
programming puzzles!  77.777778% more levels than Human Resource Machine.
@item
A whole new programming language to enjoy!  Where Human Resource Machine was
based on Assembly and executed by a single worker, 7 Billion Humans has an all
new language that lots of workers can all execute at the same time.
@item
You'll be taught everything you need to know.  Even useless skills can be put
to work!
@item
Feeling stressed out?  There are now friendly hint and \"skip\" systems to
facilitate your career's ascent.
@item
Incomprehensible cutscenes!  You will be delighted and bewildered.
@end itemize")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
