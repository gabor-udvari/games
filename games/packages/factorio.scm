;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages factorio)
  #:use-module (games utils)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (nonguix download)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg))

;; This file is heavily inspired by the corresponding package in Nixpkgs.

(define factorio-help
  (string-append
    "FETCH FAILED
============

Please ensure you have set the username and token in `"
    (guix-gaming-channel-games-config)
    "`.
Your token can be seen at https://factorio.com/profile (after logging in).  It is
not as sensitive as your password, but should still be safeguarded.  There is a
link on that page to revoke/invalidate the token, if you believe it has been
leaked or wish to take precautions.

Example content for the file:

```scheme
((game1 ...)
 ...
 (factorio .
   ((username . \"johnsmith\")
    (token    . \"c40c9636e1ba61261a50e49a035fff1d\")))
 ...
 (gameN ...))
```"))

(define stable-version "1.1.101")
(define experimental-version "1.1.104")

(define* (factorio-url-fetch url hash-algo hash
                            #:optional name
                            #:key (system (%current-system))
                            (guile (default-guile)))
  "Return a fixed-output derivation that fetches URL (a string) which is
expected to have HASH of type HASH-ALGO (a symbol).  By default, the file name
is the base name of URL; optionally, NAME can specify a different file name.

This derivation will download URL and add the username and token as defined in
the configuration of the games channel."
  (define file-name (if name name (basename url)))
  (let* ((factorio-config (read-config factorio-help 'factorio))
         (username (assoc-ref factorio-config 'username))
         (token (assoc-ref factorio-config 'token)))
    (unredistributable-url-fetch
      (string-append url "?username=" username "&token=" token)
      hash-algo hash file-name #:system system #:guile guile)))

(define-public factorio
  (package
    (name "factorio")
    (version stable-version)
    (source (origin
             (method factorio-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 version "/alpha/linux64"))
             (file-name (string-append name "-" version ".tar.xz"))
             (sha256
              (base32
               "07f8hcyf4hmf9lpa2ljm6ygpaaj2yd28da4krwa5yzjvqs88b4fq"))))
    (build-system trivial-build-system)
    (supported-systems '("x86_64-linux"))
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let* ((source (assoc-ref %build-inputs "source"))
                          (tar (string-append
                                 (assoc-ref %build-inputs "tar")
                                 "/bin/tar"))
                          (xz (assoc-ref %build-inputs "xz"))
                          (bash (assoc-ref %build-inputs "bash"))
                          (glibc (assoc-ref %build-inputs "glibc"))
                          (patchelf (assoc-ref %build-inputs "patchelf"))
                          (patchelf (string-append patchelf "/bin/patchelf"))
                          (output (assoc-ref %outputs "out"))
                          (bindir (string-append output "/bin"))
                          (datadir (string-append output "/share/factorio"))
                          (docdir (string-append output "/share/doc/factorio"))
                          (statedir "$HOME/.factorio/write-data"))
                     (mkdir-p output)
                     (setenv "PATH" (string-append (getenv "PATH") ":" xz "/bin"))
                     (invoke tar "xf" source)

                     (mkdir-p bindir)
                     (mkdir-p docdir)
                     (mkdir-p datadir)
                     (copy-recursively "factorio/bin" bindir)
                     (copy-recursively "factorio/doc-html" docdir)
                     (copy-recursively "factorio/data" datadir)

                     (let* ((factorio (string-append bindir "/x64/factorio"))
                            (symlink (string-append bindir "/factorio"))
                            (update-sh (string-append bindir "/x64/update-config.sh"))
                            (base-config (string-append datadir "/config-base.cfg")))
                       (with-output-to-file update-sh
                         (lambda _
                           (format #t "#!~a/bin/bash~%" bash)
                           (format #t "if [[ -e $HOME/.factorio/config.cfg ]]; then~%")
                           ;; Config file exists, but may have wrong path.
                           ;; Try to edit it. Nix packager seems to think this is
                           ;; fine...
                           (format #t "  sed -i 's|^read-data=.*|read-data=~a|' $HOME/.factorio/config.cfg~%"
                                   datadir)
                           (format #t "else~%")
                           ;; Config file does not exist. Phew.
                           (format #t "  install -D ~a $HOME/.factorio/config.cfg~%"
                                   base-config)
                           (format #t "fi")))
                       (chmod update-sh #o755)
                       (with-output-to-file base-config
                         (lambda _
                           (format #t "use-system-read-write-data-directories=true~%")
                           (format #t "[path]~%")
                           (format #t "read-data=~a/~%" datadir)
                           (format #t "write-data=~a/~%" statedir)
                           (format #t "[other]~%")
                           (format #t "check_updates=false~%")))
                       (with-output-to-file symlink
                         (lambda _
                           (format #t "#!~a/bin/bash~%" bash)
                           (format #t "~a/bin/x64/update-config.sh~%" output)
                           (format #t "exec ~a/bin/x64/factorio -c $HOME/.factorio/config.cfg $@"
                                   output)))
                       (chmod symlink #o755)

                       (invoke patchelf "--set-interpreter"
                               (string-append glibc "/lib/ld-linux-x86-64.so.2")
                               factorio)
                       (let ((rpath (string-join
                                      (map
                                        (lambda (input)
                                          (string-append
                                            (assoc-ref %build-inputs input)
                                            "/lib"))
                                        '("alsa-lib" "gcc" "glibc" "libice"
                                          "libsm" "libx11" "libxcursor"
                                          "libxext" "libxinerama" "libxrandr"
                                          "mesa" "pulseaudio"))
                                      ":")))
                       (invoke patchelf "--set-rpath" rpath factorio)))))))
    (native-inputs
     `(("patchelf" ,patchelf)
       ("tar" ,tar)
       ("xz" ,xz)))
    (inputs
     `(("alsa-lib" ,alsa-lib)
       ("bash" ,bash)
       ("gcc" ,gcc "lib")
       ("glibc" ,glibc)
       ("libice" ,libice)
       ("libsm" ,libsm)
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("libxinerama" ,libxinerama)
       ("libxrandr" ,libxrandr)
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)))
    (home-page "https://factorio.com")
    (synopsis "Factory building game")
    (description "Factorio is a game in which you build and maintain factories.

You will be mining resources, researching technologies, building infrastructure,
automating production and fighting enemies.  Use your imagination to design your
factory, combine simple elements into ingenious structures, apply management
skills to keep it working, and protect it from the creatures who don't really
like you.")
    (license (undistributable "https://factorio.com/terms-of-service"))))

(define-public factorio-experimental
  (package
    (inherit factorio)
    (version experimental-version)
    (source (origin
             (method factorio-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 version "/alpha/linux64"))
             (file-name (string-append "factorio-" version ".tar.xz"))
             (sha256
              (base32
               "0aizllbfzbn2j0560n4f823jqq7v7qz813an4wlm39rfsfx7b0vq"))))))

(define-public factorio-headless
  (package
    (inherit factorio)
    (name "factorio-headless")
    (source (origin
             (method unredistributable-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 stable-version "/headless/linux64"))
             (file-name (string-append name "-" stable-version ".tar.xz"))
             (sha256
              (base32
               "14l3cg8swl3l7lzp44j4zk9wldzf4g23vda67wyzfyx82pvad206"))))))

(define-public factorio-headless-experimental
  (package
    (inherit factorio-headless)
    (version experimental-version)
    (source (origin
             (method unredistributable-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 version "/headless/linux64"))
             (file-name (string-append "factorio-headless-" version ".tar.xz"))
             (sha256
              (base32
               "10qmq2mw2j97s64skwr3m7hmv21h3m0r8rdhnyfrhmrxn8x3a4wf"))))))
