#+TITLE: Hacking Guix gaming packages

Games can be hard to package for Guix because they are often closed-source and
make many assumptions on the file system hierarchy.

Here follows a checklist of things to try out whenever you get stuck:

- Some binary setups (e.g. those provided by Icculus) can often be extracted
  with =unzip=.  The Mojo Setups can be extracted with =makeself-safeextract=.

- Check the references mentioned in the [[./readme.org]], your game might have
  already been packaged for another distribution.  Don't forget to look at other
  recipes and comments.  Nix recipes are often the most relevant since they
  suffer from the same file system limitation as Guix.

- List the dynamic libraries required by executables and dynamic
  libraries with the =ldd= tool (part of the =glibc= package).
  If you run an x86_64 system, you will need the i686 build of =ldd= for 32-bit
  executables and libraries:

  #+BEGIN_SRC sh
  guix build --system=i686-linux glibc
  #+END_SRC

- Many games embed free libraries but we strive to remove them and use upstream
  packages instead.  Sometimes however the game depends on a patched version of
  a library for which we don't have the source.  If a game fails mysteriously,
  try to use all bundled libraries (e.g. by prepending them to
  =LD_LIBRARY_PATH=) see if that works.

- Some libraries are not linked against and only loaded at runtime.  If no
  meaningful error is displayed, run the game with =strace= and inspect the tail
  of the output to find out which libraries are missing.  Then export the
  offenders with =LD_LIBRARY_PATH= before starting the game.

  If =strace= does not reveal anything, try exporting common libraries, like
  the Xorg libraries (=libxext=, =libxi=, etc.).

- Many games assume the value of the current directory, often to the path of the
  executable.  If the game does not start, make sure to try out a couple of
  different locations (the root, the shared data, etc.).

- Sometimes the game assumes that the current directory is writable.  If the
  output does not make this obvious, often =strace= will.  In this case, you
  must change directory to a writable location (e.g. the user's home) and
  symlink all the required store items there.

- If sound does not work and you see
  : AL lib: oss_open_playback: Could not open /dev/dsp: No such file or directory
  make sure to use the libopenal.so provided with Guix: the one embedded in the
  game is likely not to work.

  Also make sure =alsa-lib=, =openal= and maybe =pulseaudio= are in the =LD_LIBRARY_PATH=.

- To build 32-bit games, you need to specify
  : #:system "i686-linux"
  in the arguments.
  For games that /need/ both 32-bit and 64-bit binaries, you might need to use a
  32-bit =gcc= in the inputs.
  #+BEGIN_SRC scheme
  (define gcc32
    (package
      (inherit (cross-gcc "i686-unknown-linux-gnu"
                #:libc (cross-libc "i686-unknown-linux-gnu")))
      (name "gcc32")
      (properties (alist-delete 'hidden? (package-properties gcc)))))
  #+END_SRC
  In most cases however, it's easier to define 2 packages, one that contains the
  32-bit stuff using the ~#:system "i686-linux"~ option, and one containing the
  64-bit stuff as usual.

- Desktop icons: They are often included as part of the game data (look for a
  =.png= file).
  Some Windows games embed the icon into the game executable.  You can extract
  the icon with the =icoutils= package:

  #+begin_src sh
  $ wrestool  -x --output=. -t14 foo.exe
  $ icotool -x foo.exe_14_101_1024.ico
  #+end_src

  For GOG.com games that does not include icons, check out [[https://www.gogdb.org/][gogdb.org]].  On the
  game page, click details and there should be a link to the icon.

- Big games need not be completely unpacked to =/tmp=.
  This could fill the RAM if =/tmp= is a =tmpfs=; moreover, the unchanged game
  files would be copied twice, once to =/tmp= and once to =/gnu/store=.

  Instead, it's recommended to write a custom =unpack= phase which extracts
  only the files to be patched.  Then write a custom data-installation phase
  which unpacks everything to the store.

- If game controllers are not recognized out of the box, try adding =eudev= to
  =LD_LIBRARY_PATH=.
